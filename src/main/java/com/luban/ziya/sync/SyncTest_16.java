package com.luban.ziya.sync;

import org.openjdk.jol.info.ClassLayout;

import java.util.concurrent.TimeUnit;

/**
 * Created By ziya
 * 2020/9/14
 */
public class SyncTest_16 {

    public static void main(String[] args) throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);

        SyncTest_16 obj = new SyncTest_16();
        obj.test1();
//        System.out.println(ClassLayout.parseInstance(obj).toPrintable());

//        new Thread(() -> obj.test()).start();
//        TimeUnit.SECONDS.sleep(2);
//        new Thread(() -> obj.test()).start();
//        new Thread(() -> obj.test()).start();
    }

    public synchronized void test() {
        
    }

    public void test1() {
        synchronized (this) {
            System.out.println(ClassLayout.parseInstance(this).toPrintable());

            synchronized (this) {
                System.out.println(ClassLayout.parseInstance(this).toPrintable());
            }
        }
    }

    public void test2() {
        synchronized (this) {
            System.out.println(ClassLayout.parseInstance(this).toPrintable());
        }
    }
}
